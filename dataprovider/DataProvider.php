<?php

namespace Samy\DataProvider;

/**
 * Simple DataProvider implementation.
 */
class DataProvider implements DataProviderInterface
{
    use DataProviderTrait;
}
