<?php

namespace Samy\DataProvider;

/**
 * Describes DataProvider interface.
 */
interface DataProviderInterface
{
    /**
     * Retrieve lst data.
     *
     * @param string $Filename The filename.
     * @return array<string>
     */
    public static function lst(string $Filename): array;

    /**
     * Retrieve csv data.
     *
     * @param string $Filename The filename.
     * @return array<array<string,string>>
     */
    public static function csv(string $Filename): array;

    /**
     * Retrieve json data.
     *
     * @param string $Filename The filename.
     * @return array<mixed>
     */
    public static function json(string $Filename): array;
}
