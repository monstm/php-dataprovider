<?php

namespace Samy\DataProvider;

/**
 * Describes DataProvider trait.
 */
trait DataProviderTrait
{
    /**
     * Retrieve lst data.
     *
     * @param string $Filename The filename.
     * @return array<string>
     */
    public static function lst(string $Filename): array
    {
        if (!is_file($Filename)) {
            return [];
        }

        $content = file_get_contents($Filename);
        if (!is_string($content)) {
            return [];
        }

        return explode("\n", $content);
    }

    /**
     * Retrieve csv data.
     *
     * @param string $Filename The filename.
     * @return array<array<string,string>>
     */
    public static function csv(string $Filename): array
    {
        if (!is_file($Filename)) {
            return [];
        }

        $file = @fopen($Filename, "r");
        if (!is_resource($file)) {
            return [];
        }

        $ret = [];
        $metadata = [];
        $is_header = true;
        while (($data = fgetcsv($file)) !== false) {
            if ($is_header) {
                $metadata = $data;
                $is_header = false;
            } else {
                $result = [];

                foreach ($metadata as $index => $field) {
                    $key = strval($field);
                    $value = strval($data[$index] ?? "");
                    $result[$key] = $value;
                }

                array_push($ret, $result);
            }
        }

        fclose($file);

        return $ret;
    }

    /**
     * Retrieve json data.
     *
     * @param string $Filename The filename.
     * @return array<mixed>
     */
    public static function json(string $Filename): array
    {
        if (!is_file($Filename)) {
            return [];
        }

        $content = file_get_contents($Filename);
        if (!is_string($content)) {
            return [];
        }

        $json = json_decode($content, true);
        return is_array($json) ? $json : [];
    }
}
