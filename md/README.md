# PHP DataProvider

[
	![](https://badgen.net/packagist/v/samy/dataprovider/latest)
	![](https://badgen.net/packagist/license/samy/dataprovider)
	![](https://badgen.net/packagist/dt/samy/dataprovider)
	![](https://badgen.net/packagist/favers/samy/dataprovider)
](https://packagist.org/packages/samy/dataprovider)

PHP DataProvider.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require --dev samy/dataprovider
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-dataprovider>
* User Manual: <https://monstm.gitlab.io/php-dataprovider/>
* Documentation: <https://monstm.alwaysdata.net/php-dataprovider/>
* Issues: <https://gitlab.com/monstm/php-dataprovider/-/issues>
