# DataProvider

---

## DataProvider Interface

Describes DataProvider interface.

### lst

Retrieve lst data.

```php
$lst = DataProvider::lst($filename);
```

### csv

Retrieve csv data.

```php
$csv = DataProvider::csv($filename);
```

### json

Retrieve json data.

```php
$json = DataProvider::json($filename);
```
