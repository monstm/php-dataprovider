<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\DataProvider\DataProvider;

class DataProviderTest extends TestCase
{
    /**
     * Test lst.
     *
     * @return void
     */
    public function testLst(): void
    {
        $this->assertSame(
            ["Erhard", "Benetta", "Susan"],
            DataProvider::lst(__DIR__ . DIRECTORY_SEPARATOR . "person.lst")
        );
    }

    /**
     * Test csv.
     *
     * @return void
     */
    public function testCsv(): void
    {
        $this->assertSame(
            [
                [
                    "id" => "1",
                    "first-name" => "Erhard",
                    "last-name" => "Everest",
                    "age" => "19"
                ],
                [
                    "id" => "2",
                    "first-name" => "Benetta",
                    "last-name" => "Swatton",
                    "age" => "23"
                ],
                [
                    "id" => "3",
                    "first-name" => "Susan",
                    "last-name" => "Grindle",
                    "age" => "21"
                ]
            ],
            DataProvider::csv(__DIR__ . DIRECTORY_SEPARATOR . "person.csv")
        );
    }

    /**
     * Test json.
     *
     * @return void
     */
    public function testJson(): void
    {
        $this->assertSame(
            [
                "person-1" => [
                    "first-name" => "Erhard",
                    "last-name" => "Everest",
                    "age" => 19
                ],
                "person-2" => [
                    "first-name" => "Benetta",
                    "last-name" => "Swatton",
                    "age" => 23
                ],
                "person-3" => [
                    "first-name" => "Susan",
                    "last-name" => "Grindle",
                    "age" => 21
                ]
            ],
            DataProvider::json(__DIR__ . DIRECTORY_SEPARATOR . "person.json")
        );
    }
}
